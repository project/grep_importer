<?php $previous_depth = 0; ?>
<?php $first = true; ?>
<?php $previous = null; ?>
<?php $gjennonmgaaende_vid = variable_get('grepimporter_laereplan_taxonomy', _grepimporter_get_suggested_vid('plan')) ?>

<ul class="navTabs">

    <?php foreach ($laereplaner_tree as $lp_item) : ?>

        <?php if (!$lp_item->node_count && $lp_item->depth > 0) continue; ?>

        <?php $depth = $lp_item->depth; ?>

        <?php if ($depth < $previous_depth) : ?>

            <?php while ($depth < $previous_depth) : ?>

                        </li>
                    </ul>
                </div>

                <?php $depth++; ?>

            <?php endwhile; ?>

        <?php elseif ($depth > $previous_depth) : ?>

            <div class="children<?= ($gjennonmgaaende_vid == $lp_item->vid) ? " childrenLP" : "" ?>">
                <ul>

            <?php if (($depth == 1) && ($gjennonmgaaende_vid == $lp_item->vid)) : ?>

                <?php /*

                TODO: Refactor!

                This is the gjennomgående block -- we link to the 2nd argument of the view only using the format:
                /all/<gjennongående_branch_tid>-<top_level_lp_tid>. The first part of the parameter is the tid of the
                term within the gjennongående subtree that is being displayed here, and the second part is the tid of
                the top-level taxonomy item within the LP taxonomy we want to filter gjennomgående by (i.e. Grunnskole,
                Yrkesfaglig utdanningsprogram, etc).

                NOTE that this currently only functions with the <gjennongående_branch_tid> params as being direct
                descendents of the Gjennomgående Læreplaner top-level.

                */ ?>

                <li class="gjennomegaendeLps">
                    <p>Gjennomgående læreplaner</p>
                    <ul>
                        <?php $related_lps = taxonomy_get_related($previous->tid); ?>
                        <?php $printed_rels = array(); ?>

                        <?php foreach ($related_lps as $related_tid => $related) : ?>

                            <?php $rel_parents = taxonomy_get_parents($related_tid); ?>
                            <?php $rel_parent = array_shift($rel_parents); ?>
                            <?php $rel_parents = taxonomy_get_parents($rel_parent->tid); ?>
                            <?php $rel_parent = array_shift($rel_parents); ?>

                            <?php $selected = ($rel_parent->tid == $gjennongaaende_branch_tid) && ($previous->tid == $top_level_lp_tid); ?>

                            <?php if (!in_array($rel_parent->tid, $printed_rels) && in_array($rel_parent->tid, array_keys($gjennomgaende_tree))) : ?>

                                <?php array_push($printed_rels, $rel_parent->tid); ?>

                                <li<?php if ($selected) : ?> class="selected"<?php endif; ?>>
                                    <div class="entry">
                                        <a href="/<?= $basepath ?>/all/<?= $rel_parent->tid ?>-<?= $previous->tid ?>"><?= check_plain($rel_parent->name) ?></a>
                                    </div>
                                </li>

                            <?php endif; ?>

                        <?php endforeach; ?>

                    </ul>
                </li>

            <?php endif;?>

            <?php $first = true; ?>

        <?php endif; ?>

        <?php if (!$first) : ?>

            </li>

        <?php endif; ?>

        <li class="depth<?= $lp_item->depth ?><?php if ($lp_item->tid == $current) : ?> selected<?php endif; ?>">
            <div class="entry">
                <a href="/<?= $basepath ?>/<?= $lp_item->tid ?>"><?= check_plain($lp_item->name) ?></a>
                <?php if ($lp_item->depth > 0) :?>
                    <span class="counter">(<?= $lp_item->node_count ?>)</span>
                <?php endif; ?>
            </div>

        <?php $first = false; ?>
        <?php $previous_depth = $lp_item->depth; ?>
        <?php $previous = $lp_item; ?>

    <?php endforeach; ?>

    <?php while ($previous_depth > 0) : ?>

                </li>
            </ul>
        </div>

        <?php $previous_depth--; ?>

    <?php endwhile; ?>

    </li>
</ul>
