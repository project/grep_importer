<?php
/**
 * Argument handler for related taxonomy terms with depth. This is a modified copy of
 * views_handler_argument_term_node_tid_depth.
 *
 * TODO: Review the form options
 * TODO: Review (or remove?) the breadcrumb
 */
class grepimporter_handler_argument_rel_term_node_tid_depth extends views_handler_argument {
  function option_definition() {
    $options = parent::option_definition();

    $options['depth'] = array('default' => 0);
    $options['break_phrase'] = array('default' => FALSE);
    $options['set_breadcrumb'] = array('default' => FALSE);
    $options['use_taxonomy_term_path'] = array('default' => FALSE);

    return $options;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    $form['depth'] = array(
      '#type' => 'weight',
      '#title' => t('Depth'),
      '#default_value' => $this->options['depth'],
      '#description' => t('The depth will match nodes tagged with terms in the hierarchy. For example, if you have the term "fruit" and a child term "apple", with a depth of 1 (or higher) then filtering for the term "fruit" will get nodes that are tagged with "apple" as well as "fruit". If negative, the reverse is true; searching for "apple" will also pick up nodes tagged with "fruit" if depth is -1 (or lower).'),
    );

    $form['break_phrase'] = array(
      '#type' => 'checkbox',
      '#title' => t('Allow multiple terms per argument'),
      '#description' => t('If selected, users can enter multiple arguments in the form of 1+2+3. Due to the number of JOINs it would require, AND will be treated as OR with this argument.'),
      '#default_value' => !empty($this->options['break_phrase']),
    );

    $form['set_breadcrumb'] = array(
      '#type' => 'checkbox',
      '#title' => t("Set the breadcrumb for the term parents"),
      '#description' => t('If selected, the breadcrumb trail will include all parent terms, each one linking to this view. Note that this only works if just one term was received.'),
      '#default_value' => !empty($this->options['set_breadcrumb']),
    );

    $form['use_taxonomy_term_path'] = array(
      '#type' => 'checkbox',
      '#title' => t("Use Drupal's taxonomy term path to create breadcrumb links"),
      '#description' => t('If selected, the links in the breadcrumb trail will be created using the standard drupal method instead of the custom views method. This is useful if you are using modules like taxonomy redirect to modify your taxonomy term links.'),
      '#default_value' => !empty($this->options['use_taxonomy_term_path']),
      '#process' => array('views_process_dependency'),
      '#dependency' => array('edit-options-set-breadcrumb' => array(TRUE)),
    );
  }

  function set_breadcrumb(&$breadcrumb) {
    if (empty($this->options['set_breadcrumb']) || !is_numeric($this->argument)) {
      return;
    }

    return views_taxonomy_set_breadcrumb($breadcrumb, $this);
  }

  /**
   * Override default_actions() to remove summary actions.
   */
  function default_actions($which = NULL) {
    if ($which) {
      if (in_array($which, array('ignore', 'not found', 'empty', 'default'))) {
        return parent::default_actions($which);
      }
      return;
    }
    $actions = parent::default_actions();
    unset($actions['summary asc']);
    unset($actions['summary desc']);
    return $actions;
  }

  function query() {
    $this->ensure_my_table();

    // The argument is passed in the format "<gjennongående_branch_tid>-<top_level_lp_tid>"

    $args = split("-", $this->argument);

    $gjennomgaaende_lp = $args[0];
    $related_top_level = $args[1];

    // This is, at best, a small nightmare. We need to retrieve the nodes linked with the taxonomy hierarchy levels
    // under the Gjennomgående LP specified by $gjennomgaaende_lp, but only those within the Gjennomgående subtree
    // associated with the top level $related_top_level. These associations are normal Drupal "related terms" between
    // Gjennomgående LP sub-items and top-level LP groups, representing the part of a Gjennomgående LP that applies to
    // a period of study (Grunnskole, etc).
    //
    // NOTE: We assume that these related terms only exist on terms that are immediate children of $gjennomgaaende_lp

    $subquery = "\n  SELECT DISTINCT\n";

    // This consolidates the joined term_hierarchy>term_node into a single field

    for ($i = $this->options['depth']; $i > 1; --$i) {

        $subquery .= "    IF(tn$i.vid, tn$i.vid, ";
    }

    $subquery .= "tn1.vid" . str_repeat(")", $this->options['depth'] - 1) . " AS vid \n";
    $subquery .= "    FROM {term_hierarchy} th1\n";

    // Pick the children of $gjennomgaaende_lp that are related to $related_top_level only (see NOTE above)

    $subquery .= "    LEFT JOIN {term_hierarchy} th_children1 ON th_children1.parent = th1.tid\n";
    $subquery .= "    LEFT JOIN {term_hierarchy} th_children2 ON th_children2.parent = th_children1.tid\n";
    $subquery .= "    LEFT JOIN {term_relation} tr ON tr.tid1 = th_children2.tid\n";

    // And now pick nodes associated with terms below those children

    $subquery .= "    LEFT JOIN {term_node} tn1 ON tn1.tid = th_children2.tid\n";
    $subquery .= "    LEFT JOIN {term_hierarchy} th2 ON th2.parent = th_children2.tid\n";
    $subquery .= "    LEFT JOIN {term_node} tn2 ON tn2.tid = th2.tid\n";
    $where     = "    WHERE tr.tid2 = $related_top_level AND (th1.tid = $gjennomgaaende_lp)\n";

    // And keep doing so for as many levels as defined

    for ($i = 3; $i <= ($this->options['depth']); ++$i) {

        $subquery .= "    LEFT JOIN {term_hierarchy} th$i ON th$i.parent = th" . ($i - 1) . ".tid\n";
        $subquery .= "    LEFT JOIN {term_node} tn$i ON tn$i.tid = th$i.tid\n";
    }

    $this->query->add_where(0, "$this->table_alias.$this->real_field IN ($subquery$where  )", $args);
  }

  function title() {
    $term = taxonomy_get_term($this->argument);
    if (!empty($term)) {
      return check_plain($term->name);
    }
    // TODO review text
    return t('No name');
  }
}
